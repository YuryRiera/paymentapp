package yriera.com.paymentapp.activity;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.Serializable;

import yriera.com.paymentapp.R;
import yriera.com.paymentapp.fragment.FragmentAmount;
import yriera.com.paymentapp.fragment.FragmentBank;
import yriera.com.paymentapp.fragment.FragmentFee;
import yriera.com.paymentapp.fragment.FragmentPaymentMethod;
import yriera.com.paymentapp.utils.Utility;
import yriera.com.paymentapp.utils.WebServiceCall;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setMainActivityFragmentShow(Utility.FRAGMENT_AMOUNT);
    }

    public void setMainActivityFragmentShow(String fragmentToShow) {
        setMainActivityFragmentShow(fragmentToShow, false);
    }


    public void setMainActivityFragmentShow(String fragmentToShow, boolean isFinish){

        FragmentTransaction ft = getFragmentManager().beginTransaction();

        switch (fragmentToShow){

            case Utility.FRAGMENT_AMOUNT:

                FragmentAmount fragmentAmount = new FragmentAmount();
                Bundle args = new Bundle();
                args.putBoolean("isFinish", isFinish);
                fragmentAmount.setArguments(args);

                ft.replace(R.id.mainActivityFragment, fragmentAmount);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();

                break;

            case Utility.FRAGMENT_PAYMENT_METHOD:

                ft.replace(R.id.mainActivityFragment, new FragmentPaymentMethod());
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();

                break;

            case Utility.FRAGMENT_BANK:

                ft.replace(R.id.mainActivityFragment, new FragmentBank());
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();

                break;

            case Utility.FRAGMENT_FEE:

                ft.replace(R.id.mainActivityFragment, new FragmentFee());
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();

                break;
        }
    }
}
