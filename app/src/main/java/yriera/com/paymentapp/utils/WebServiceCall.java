package yriera.com.paymentapp.utils;

import android.app.Fragment;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Query;
import yriera.com.paymentapp.fragment.FragmentBank;
import yriera.com.paymentapp.fragment.FragmentFee;
import yriera.com.paymentapp.fragment.FragmentPaymentMethod;
import yriera.com.paymentapp.object.BankItemObject;
import yriera.com.paymentapp.object.FeeItemObject;
import yriera.com.paymentapp.object.PaymentMethodItemObject;
import yriera.com.paymentapp.provider.PaymentProvider;
import yriera.com.paymentapp.webservice.ServiceGenerator;

public class WebServiceCall {

    public static void getPaymentMethods(final Fragment fragment) {

        PaymentProvider service = ServiceGenerator.createService(PaymentProvider.class);

        Utility.errorLog("getPaymentMethod | " + Utility.PUBLIC_KEY);

        Call<ArrayList<PaymentMethodItemObject>> call = service.getPaymentMethods(
                Utility.PUBLIC_KEY
        );

        call.enqueue(new Callback<ArrayList<PaymentMethodItemObject>>() {

            @Override
            public void onResponse(Call<ArrayList<PaymentMethodItemObject>> call, Response<ArrayList<PaymentMethodItemObject>> response) {
                Utility.errorLog("getPaymentMethod | response | " + response.toString());
                ArrayList<PaymentMethodItemObject> result;

                if (response.isSuccessful()) {
                    result = response.body();
                    Utility.errorLog("getPaymentMethod | result | " + result.toString());

                    ((FragmentPaymentMethod) fragment).paintPaymentMethods(result);

                } else {

                    Utility.errorLog("getPaymentMethod | showErrorResponse...");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PaymentMethodItemObject>> call, Throwable t) {
                Utility.errorLog("getPaymentMethod | onFailure | " + t.getMessage());
            }
        });
    }

    public static void getCardIssuers(final Fragment fragment, String paymentMethodId) {

        PaymentProvider service = ServiceGenerator.createService(PaymentProvider.class);

        Utility.errorLog("getCardIssuers | " + Utility.PUBLIC_KEY);

        Call<ArrayList<BankItemObject>> call = service.getCardIssuers(
                Utility.PUBLIC_KEY,
                paymentMethodId
        );

        call.enqueue(new Callback<ArrayList<BankItemObject>>() {

            @Override
            public void onResponse(Call<ArrayList<BankItemObject>> call, Response<ArrayList<BankItemObject>> response) {
                Utility.errorLog("getCardIssuers | response | " + response.toString());
                ArrayList<BankItemObject> result;

                if (response.isSuccessful()) {
                    result = response.body();
                    Utility.errorLog("getCardIssuers | result | " + result.toString());

                    ((FragmentBank) fragment).paintBanks(result);

                } else {

                    Utility.errorLog("getCardIssuers | showErrorResponse...");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BankItemObject>> call, Throwable t) {
                Utility.errorLog("getCardIssuers | onFailure | " + t.getMessage());
            }
        });
    }

    public static void getInstallments(final Fragment fragment, String amount, String paymentMethodId, String issuerId) {

        PaymentProvider service = ServiceGenerator.createService(PaymentProvider.class);

        Utility.errorLog("getCardIssuers | PUBLIC_KEY | " + Utility.PUBLIC_KEY);
        Utility.errorLog("getCardIssuers | amount | " + amount);
        Utility.errorLog("getCardIssuers | paymentMethodId | " + paymentMethodId);
        Utility.errorLog("getCardIssuers | issuerId | " + issuerId);

        Call<ArrayList<FeeItemObject>> call = service.getInstallments(
                Utility.PUBLIC_KEY,
                amount,
                paymentMethodId,
                issuerId
        );

        call.enqueue(new Callback<ArrayList<FeeItemObject>>() {

            @Override
            public void onResponse(Call<ArrayList<FeeItemObject>> call, Response<ArrayList<FeeItemObject>> response) {
                Utility.errorLog("getCardIssuers | response | " + response.toString());
                ArrayList<FeeItemObject> result;

                if (response.isSuccessful()) {
                    result = response.body();
                    Utility.errorLog("getCardIssuers | result | " + result.toString());

                    ((FragmentFee) fragment).paintMessages(result);

                } else {

                    Utility.errorLog("getCardIssuers | showErrorResponse...");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FeeItemObject>> call, Throwable t) {
                Utility.errorLog("getCardIssuers | onFailure | " + t.getMessage());
            }
        });
    }
}