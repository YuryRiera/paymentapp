package yriera.com.paymentapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Utility {

    private static final boolean isTesterEnviroment = true;

    public static final String URL = "https://api.mercadopago.com/v1/";
    public static final String PUBLIC_KEY= "444a9ef5-8a6b-429f-abdf-587639155d88";

    public static final String FRAGMENT_PAYMENT_METHOD = "fragment_payment_method";
    public static final String FRAGMENT_AMOUNT = "fragment_amount";
    public static final String FRAGMENT_BANK = "fragment_bank";
    public static final String FRAGMENT_FEE = "fragment_fee";

    public static final String CREDIT_CARD_TYPE = "credit_card";
    public static final String GLOBAL_DATA = "global_data";

    public static final String AMOUNT = "amount";
    public static final String PAYMENT_METHOD_ID = "payment_method_id";
    public static final String BANK_ID = "bank_id";
    public static final String BANK_NAME = "bank_name";
    public static final String FEES = "fees";

    public static SharedPreferences mPref;

    public static void errorLog(String msg){

        if(isTesterEnviroment){
            Log.e("MLB:-", msg);
        }

    }

    public static void clearData(Context mContext, String clearDataString){
        if(mContext != null){
            mPref = mContext.getSharedPreferences(clearDataString, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = mPref.edit();
            editor.clear();
            editor.commit();
        }
    }

    public static void setData(Context mContext, String key, String value){
        if(mContext != null){
            mPref = mContext.getSharedPreferences(GLOBAL_DATA, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = mPref.edit();
            editor.putString(key, value);
            editor.commit();
        }

    }

    public static String getData(Context mContext, String key){
        String text = null;

        if(mContext != null){
            mPref = mContext.getSharedPreferences(GLOBAL_DATA, Context.MODE_PRIVATE);
            text = mPref.getString(key, null);
        }

        return text;
    }
}

