package yriera.com.paymentapp.adapter;


import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.signature.StringSignature;

import java.util.ArrayList;

import yriera.com.paymentapp.R;
import yriera.com.paymentapp.object.PaymentMethodItemObject;
import yriera.com.paymentapp.utils.Utility;


public class RecyclerViewPaymentMethodAdapter extends RecyclerView.Adapter<RecyclerViewPaymentMethodAdapter.MyViewHolder> {

    private ArrayList<PaymentMethodItemObject> paymentMethods = new ArrayList<>();
    private final OnItemClickListener listener;
    private Activity mActivity;

    public interface OnItemClickListener {
        void onItemClick(PaymentMethodItemObject item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView name;


        public MyViewHolder(View view) {
            super(view);

            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);

        }

        public void bind(final PaymentMethodItemObject item, final OnItemClickListener listener) {

            Utility.errorLog("item **** " + item.toString());
            name.setText(item.getName());

            Glide.with(mActivity)
                    .load(item.getImage())
                    .asBitmap()
                    .centerCrop()
                    //.placeholder(R.drawable.defult_image)
                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                    .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
                    .into(new BitmapImageViewTarget(image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            image.setImageBitmap(resource);
                        }
                    });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public RecyclerViewPaymentMethodAdapter(Activity mActivity, ArrayList<PaymentMethodItemObject> paymentMethods, OnItemClickListener listener) {
        this.listener = listener;
        this.paymentMethods = paymentMethods;
        this.mActivity = mActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_payment_method_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(paymentMethods.get(position), listener);
    }

    @Override
    public int getItemCount() {

        return paymentMethods.size();
    }

}