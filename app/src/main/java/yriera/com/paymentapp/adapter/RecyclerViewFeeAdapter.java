package yriera.com.paymentapp.adapter;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import yriera.com.paymentapp.R;
import yriera.com.paymentapp.object.PayerCostsItemObject;
import yriera.com.paymentapp.utils.Utility;


public class RecyclerViewFeeAdapter extends RecyclerView.Adapter<RecyclerViewFeeAdapter.MyViewHolder> {

    private ArrayList<PayerCostsItemObject> fees = new ArrayList<>();
    private final OnItemClickListener listener;
    private Activity mActivity;

    public interface OnItemClickListener {
        void onItemClick(PayerCostsItemObject item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView name;
        public LinearLayout llImage;


        public MyViewHolder(View view) {
            super(view);

            llImage = view.findViewById(R.id.llImage);
            llImage.setVisibility(View.GONE);

            name = view.findViewById(R.id.name);
        }

        public void bind(final PayerCostsItemObject item, final OnItemClickListener listener) {

            Utility.errorLog("item **** " + item.toString());
            name.setText(item.getMessages());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public RecyclerViewFeeAdapter(Activity mActivity, ArrayList<PayerCostsItemObject> fees, OnItemClickListener listener) {
        this.listener = listener;
        this.fees = fees;
        this.mActivity = mActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_payment_method_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(fees.get(position), listener);
    }

    @Override
    public int getItemCount() {

        return fees.size();
    }

}