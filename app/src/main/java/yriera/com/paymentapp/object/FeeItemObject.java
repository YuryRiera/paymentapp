package yriera.com.paymentapp.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class FeeItemObject implements Serializable {

    @SerializedName("payer_costs")
    @Expose
    private ArrayList<PayerCostsItemObject> payerCosts;


    public ArrayList getPayerCosts() {
        return payerCosts;
    }

    public void setPayerCosts(ArrayList payerCosts) {
        this.payerCosts = payerCosts;
    }

    @Override
    public String toString() {
        return "{" +
                "payerCosts=" + payerCosts +
                '}';
    }
}