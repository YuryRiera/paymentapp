package yriera.com.paymentapp.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class PayerCostsItemObject implements Serializable {

    @SerializedName("recommended_message")
    @Expose
    private String messages;

    public PayerCostsItemObject() {
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    @Override
    public String toString() {
        return "PayerCostsItemObject{" +
                "messages='" + messages + '\'' +
                '}';
    }
}