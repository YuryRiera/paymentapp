package yriera.com.paymentapp.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import yriera.com.paymentapp.R;
import yriera.com.paymentapp.activity.MainActivity;
import yriera.com.paymentapp.utils.Utility;

public class FragmentAmount extends Fragment {

    private static AlertDialog alertDialog;

    public FragmentAmount() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_amount, container, false);

        final EditText ammount = view.findViewById(R.id.ammount);
        TextView nextStep = view.findViewById(R.id.nextStep);

        Bundle bundle = this.getArguments();

        if (bundle != null) {

            Utility.errorLog("FragmentAmount | isFinish ******************* " + bundle.getBoolean("isFinish", false));

            if(bundle.getBoolean("isFinish", false)){

                showAlertDialog();
            }
        }

        nextStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ammount.getText().toString().length() > 0){
                    ammount.setError(null);

                    Utility.setData(getActivity(), Utility.AMOUNT, ammount.getText().toString());
                    ((MainActivity) getActivity()).setMainActivityFragmentShow(Utility.FRAGMENT_PAYMENT_METHOD);

                }else{

                    ammount.setError("Para continuar debe agregar un monto");
                }
            }
        });

        return view;
    }

    private void showAlertDialog(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = ((Activity) getActivity()).getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.popup_dialog_error, null);
        builder.setView(dialogView);

        TextView amount = dialogView.findViewById(R.id.amount);
        TextView paymentMethod = dialogView.findViewById(R.id.paymentMethod);
        TextView bankName = dialogView.findViewById(R.id.bankName);
        TextView feeMessage = dialogView.findViewById(R.id.feeMessage);

        amount.setText(Utility.getData(getActivity(), Utility.AMOUNT));
        paymentMethod.setText(Utility.getData(getActivity(), Utility.PAYMENT_METHOD_ID));
        bankName.setText(Utility.getData(getActivity(), Utility.BANK_NAME));
        feeMessage.setText(Utility.getData(getActivity(), Utility.FEES));

        Button okButton = dialogView.findViewById(R.id.btn_ok);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utility.clearData(getActivity(), Utility.GLOBAL_DATA);
                alertDialog.dismiss();

            }
        });

        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}