package yriera.com.paymentapp.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;

import yriera.com.paymentapp.R;
import yriera.com.paymentapp.activity.MainActivity;
import yriera.com.paymentapp.adapter.RecyclerViewBankAdapter;
import yriera.com.paymentapp.adapter.RecyclerViewFeeAdapter;
import yriera.com.paymentapp.object.BankItemObject;
import yriera.com.paymentapp.object.FeeItemObject;
import yriera.com.paymentapp.object.PayerCostsItemObject;
import yriera.com.paymentapp.utils.Utility;
import yriera.com.paymentapp.utils.WebServiceCall;


public class FragmentFee extends Fragment {

    private View view;

    public FragmentFee() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_fee, container, false);

        WebServiceCall.getInstallments(this,
                Utility.getData(getActivity(), Utility.AMOUNT),
                Utility.getData(getActivity(), Utility.PAYMENT_METHOD_ID),
                Utility.getData(getActivity(), Utility.BANK_ID));

        return view;
    }

    public void paintMessages(ArrayList<FeeItemObject> fees){

        Utility.errorLog(" | TEST | " + fees.toString());

        RecyclerView mRecyclerView = view.findViewById(R.id.rvFee);
        LinearLayout llNoBanks = view.findViewById(R.id.llNoBanks);

        if(fees.size() > 0){

            llNoBanks.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);

            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

            mRecyclerView.setLayoutManager(mLayoutManager);

            RecyclerViewFeeAdapter mAdapter = new RecyclerViewFeeAdapter(getActivity(), fees.get(0).getPayerCosts(), new RecyclerViewFeeAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(PayerCostsItemObject item) {
                    Utility.setData(getActivity(), Utility.FEES, item.getMessages());
                    ((MainActivity) getActivity()).setMainActivityFragmentShow(Utility.FRAGMENT_AMOUNT, true);
                }
            });

            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }else{

            mRecyclerView.setVisibility(View.GONE);
            llNoBanks.setVisibility(View.VISIBLE);

            llNoBanks.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getFragmentManager().popBackStack();
                }
            });

        }

    }
}