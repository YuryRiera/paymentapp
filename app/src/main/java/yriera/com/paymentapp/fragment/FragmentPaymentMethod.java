package yriera.com.paymentapp.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import yriera.com.paymentapp.R;
import yriera.com.paymentapp.activity.MainActivity;
import yriera.com.paymentapp.adapter.RecyclerViewPaymentMethodAdapter;
import yriera.com.paymentapp.object.PaymentMethodItemObject;
import yriera.com.paymentapp.utils.Utility;
import yriera.com.paymentapp.utils.WebServiceCall;


public class FragmentPaymentMethod extends Fragment {

    private View view;

    public FragmentPaymentMethod() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_payment_method, container, false);

        WebServiceCall.getPaymentMethods(this);

        return view;
    }

    public void paintPaymentMethods(ArrayList<PaymentMethodItemObject> paymentMethods){

        Utility.errorLog(" | HELLO | " + paymentMethods.toString());


        for (int i = 0; i < paymentMethods.size(); i++) {
            if(!paymentMethods.get(i).getPaymentTypeId().equalsIgnoreCase(Utility.CREDIT_CARD_TYPE)){
                paymentMethods.remove(i);
            }
        }

        if(paymentMethods.size() > 0){

            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

            RecyclerView mRecyclerView = view.findViewById(R.id.rvPaymentMethod);
            mRecyclerView.setLayoutManager(mLayoutManager);

            RecyclerViewPaymentMethodAdapter mAdapter = new RecyclerViewPaymentMethodAdapter(getActivity(), paymentMethods, new RecyclerViewPaymentMethodAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(PaymentMethodItemObject item) {
                    Utility.setData(getActivity(), Utility.PAYMENT_METHOD_ID, item.getId());
                    ((MainActivity) getActivity()).setMainActivityFragmentShow(Utility.FRAGMENT_BANK);
                }
            });

            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }

    }
}