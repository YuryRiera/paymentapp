package yriera.com.paymentapp.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;

import yriera.com.paymentapp.R;
import yriera.com.paymentapp.activity.MainActivity;
import yriera.com.paymentapp.adapter.RecyclerViewBankAdapter;
import yriera.com.paymentapp.object.BankItemObject;
import yriera.com.paymentapp.utils.Utility;
import yriera.com.paymentapp.utils.WebServiceCall;


public class FragmentBank extends Fragment {

    private View view;

    public FragmentBank() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_bank, container, false);

        WebServiceCall.getCardIssuers(this, Utility.getData(getActivity(), Utility.PAYMENT_METHOD_ID));

        return view;
    }

    public void paintBanks(ArrayList<BankItemObject> banks){

        Utility.errorLog(" | TEST | " + banks.toString());

        RecyclerView mRecyclerView = view.findViewById(R.id.rvBanks);
        LinearLayout llNoBanks = view.findViewById(R.id.llNoBanks);

        if(banks.size() > 0){

            llNoBanks.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);

            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

            mRecyclerView.setLayoutManager(mLayoutManager);

            RecyclerViewBankAdapter mAdapter = new RecyclerViewBankAdapter(getActivity(), banks, new RecyclerViewBankAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BankItemObject item) {
                    Utility.setData(getActivity(), Utility.BANK_ID, item.getId());
                    Utility.setData(getActivity(), Utility.BANK_NAME, item.getName());
                    ((MainActivity) getActivity()).setMainActivityFragmentShow(Utility.FRAGMENT_FEE);
                }
            });

            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }else{

            mRecyclerView.setVisibility(View.GONE);
            llNoBanks.setVisibility(View.VISIBLE);

            llNoBanks.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getFragmentManager().popBackStack();
                }
            });

        }

    }
}