package yriera.com.paymentapp.response;

import java.io.Serializable;
import java.util.ArrayList;

import yriera.com.paymentapp.object.PaymentMethodItemObject;

/**
 */
public class GetPaymentMethodResponse implements Serializable {

    private ArrayList<PaymentMethodItemObject> paymentMethods;

    public GetPaymentMethodResponse() {

    }

    public ArrayList<PaymentMethodItemObject> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(ArrayList<PaymentMethodItemObject> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    @Override
    public String toString() {
        return "GetPaymentMethodResponse{" +
                "paymentMethods=" + paymentMethods +
                '}';
    }
}
