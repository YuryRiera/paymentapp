package yriera.com.paymentapp.provider;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import yriera.com.paymentapp.object.BankItemObject;
import yriera.com.paymentapp.object.FeeItemObject;
import yriera.com.paymentapp.object.PaymentMethodItemObject;

public interface PaymentProvider {

    @GET("payment_methods?")
    Call<ArrayList<PaymentMethodItemObject>> getPaymentMethods(
            @Query("public_key") String key);

    @GET("payment_methods/card_issuers?")
    Call<ArrayList<BankItemObject>> getCardIssuers(
            @Query("public_key") String key,
            @Query("payment_method_id") String paymentMethodId);

    @GET("payment_methods/installments?")
    Call<ArrayList<FeeItemObject>> getInstallments(
            @Query("public_key") String key,
            @Query("amount") String amount,
            @Query("payment_method_id") String paymentMethodId,
            @Query("issuer.id") String issuerId);
}